# BHStreams-Arduino

BHStreams-Arduino is a small Arduino library for building interfacing with Ballarat Hackerspace's `Streams` service.


### Required Dependencies
* MQTT (by Joel Gehwiler - v2.4.1 or later)

### Optional Dependencies
* ArduinoJson (by Benoit Blanchon - v5.x)
* ArduinoOTA (by Ivan Grokhotkov/Miguel Angel Ajo - v1.0)