#include "BHStreams.h"

BHStreams bhs;
uint32_t lastUpdate = 0;

void onData(BHStreams_Data *data) {
  Serial.printf("received: %s\n", data->toChar());
}

void setup() {
  Serial.begin(115200);

  bhs.begin("WIFI SSID", "WIFI PASSWORD");
  bhs.connect();
  bhs.onStream("bar", onData);

  Serial.println("setup() complete.");
}

void loop() {
  // manage WiFi connection, and any MQTT or OTA messages
  bhs.process();

  // process other functions
  uint32_t now = millis();

  if (now - lastUpdate > 5000) {
    if (bhs.write("foo", "ping")) {
      Serial.println("sent ping");
    }

    lastUpdate = now;
  }
}
